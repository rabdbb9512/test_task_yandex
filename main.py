import os
import telebot
import requests
import subprocess
from telebot import types
import speech_recognition as sr
from dotenv import load_dotenv

load_dotenv()
token_bot = os.environ.get("token_bot_tg")
voice_gpt = os.environ.get("voice_gpt")
voice_sql = os.environ.get("voice_sql")
voice_history = os.environ.get("voice_history")
photo_new = os.environ.get("photo_new")
photo_old = os.environ.get("photo_old")

bot = telebot.TeleBot(token_bot)

ph = "AgACAgIAAxkBAAOAZN2hMgzpBwABWFVkoNvakzAh_9czAAJ2yzEbiwvwSpdiJ9PiYVw6AQADAgADcwADMAQ"


@bot.message_handler(commands=["start"])
def func(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton("Моё увлечение")
    btn2 = types.KeyboardButton("Посмотреть фото")
    markup.add(btn1, btn2)
    bot.send_message(
        message.chat.id,
        text="Привет, {0.first_name}! Я бот тестового задания Яндекса!\n"
        "У меня можно нажимать кнопки, отправлять команды и я даже распознаю голосовые сообщения\n"
        "Команды которые тебе доступны:\n"
        "1)Чтобы посмотреть фото нажмите кнопку'Посмотреть фото'(голосовой вызов'Посмотреть фото')\n"
        "2)/get_link - Получение ссылки на мой репозиторий(голосовой вызов'Ссылка')\n"
        "3)/get_voice - Получение моих голосовых сообщений((голосовой вызов'Голосовые'))\n"
        "4)/start - Возврат к началу, где ты можешь почитать о моем создателе\n"
        "или посмотреть его фото".format(message.from_user),
        reply_markup=markup,
    )


@bot.message_handler(commands=["get_voice"])
def get_voice(message):
    """Функция передачи голосовых"""

    bot.send_message(message.chat.id, "Сообщение о GPT".format(message.from_user))
    bot.send_voice(message.chat.id, voice=voice_gpt)
    bot.send_message(
        message.chat.id,
        "Сообщение о разнице между SQL и NoSQL".format(message.from_user),
    )
    bot.send_voice(message.chat.id, voice=voice_sql)
    bot.send_message(message.chat.id, "История".format(message.from_user))
    bot.send_voice(message.chat.id, voice=voice_history)


@bot.message_handler(commands=["get_link"])
def link(message):
    """Функция отдачи ссылки"""

    markup = types.InlineKeyboardMarkup()
    button1 = types.InlineKeyboardButton(
        "Ccылка на репозиторий",
        url="https://gitlab.com/rabdbb9512/test_task_yandex.git",
    )
    markup.add(button1)
    bot.send_message(
        message.chat.id,
        "Привет, {0.first_name}! Нажми на кнопку и перейди к репозиторию)".format(
            message.from_user
        ),
        reply_markup=markup,
    )


def audio_to_text(dest_name: str):
    """Функция для перевода аудио, в формате ".vaw" в текст"""

    r = sr.Recognizer()
    message = sr.AudioFile(dest_name)
    with message as source:
        audio = r.record(source)
    return r.recognize_google(audio, language="ru_RU")


@bot.message_handler(content_types=["text"])
def func(message):
    """Функция для распознования текстовых команд"""
    if message.text == "Моё увлечение":
        bot.send_message(
            message.chat.id,
            text="Моим главным увлечением является история.\n"
            "С детства был увлечен этой наукой, расскрывать прошлое, узнавать как жили наши предки,\n"
            "Все это очень увлекательно. Любовь к этому предмету мне привил отец, мы могли часами говорить об истории.\n"
            "Пускай моя проффессия сейчас не связана с историей, я до сих пор смотрю очень много историков и читаю различные книги",
        )

    elif message.text == "Посмотреть фото":
        bot.send_photo(message.chat.id, photo=photo_new)
        bot.send_photo(message.chat.id, photo=photo_old)

    else:
        bot.send_message(
            message.chat.id, text="На такую комманду я не запрограммирован.."
        )


@bot.message_handler(content_types=["voice"])
def get_audio_messages(message):
    """Функция для чтения голосовых сообщений"""
    try:
        # Ниже пытаемся вычленить имя файла, да и вообще берем данные
        file_info = bot.get_file(message.voice.file_id)
        # ПОлучение пути до файла (например: voice/file_2.oga)
        path = file_info.file_path

        # Преобразуем путь в имя файла (например: file_2.oga)
        fname = os.path.basename(path)
        # Получаем и сохраняем присланное голосове сообщение
        doc = requests.get(
            "https://api.telegram.org/file/bot{0}/{1}".format(
                token_bot, file_info.file_path
            )
        )
        with open(fname + ".oga", "wb") as f:
            f.write(doc.content)

        # Используем ПО ffmpeg, для конвертации .oga в .vaw
        process = subprocess.run(["ffmpeg", "-i", fname + ".oga", fname + ".wav"])
        # Вызов функции для перевода аудио в текст
        result = audio_to_text(fname + ".wav")
        # Отправляем пользователю, приславшему файл, его текст
        bot.send_message(message.from_user.id, format(result.capitalize()))
        message.text = result.capitalize()
        if message.text == "Ссылка":
            bot.register_next_step_handler(link(message))
        elif message.text == "Голосовые":
            bot.register_next_step_handler(get_voice(message))

        bot.register_next_step_handler(func(message))

    except sr.UnknownValueError as e:
        # Ошибка возникает, если сообщение не удалось разобрать. В таком случае отсылается ответ пользователю и заносим запись в лог ошибок
        bot.send_message(
            message.from_user.id,
            "Прошу прощения, но я не разобрал сообщение, или оно поустое...",
        )
    except Exception as e:
        pass
    finally:
        # В любом случае удаляем временные файлы с аудио сообщением
        os.remove(fname + ".wav")
        os.remove(fname + ".oga")


bot.polling(none_stop=True)
